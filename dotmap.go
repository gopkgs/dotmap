/*
Package dotmap provides functions to access values of
nested maps using dot notation.

	valA, err := dotmap.GetString(myMap, "myKey.nestedKey.deepNestedKey")
	valB := dotmap.GetStringDefault(myMap, "myKey.nestedKey.deepNestedKey", "This is default value")

Input map and nested maps must be of type map[string]interface{}
to get correct output.
*/
package dotmap

import (
	"errors"
	"strings"
	"time"

	"github.com/spf13/cast"
)

// Get returns raw interface{} and error if any.
func Get(m map[string]interface{}, dotkey string) (val interface{}, err error) {
	keys := strings.Split(dotkey, ".")
	lenKeys := len(keys)
	tmpmap := m
	for i, key := range keys {
		var ok bool
		if val, ok = tmpmap[key]; !ok {
			err = errors.New("Key \"" + key + "\" not found")
			return
		}
		if i != lenKeys-1 {
			var ok bool
			if tmpmap, ok = val.(map[string]interface{}); !ok {
				err = errors.New("\"" + key + "\" is not of type map[string]interface{}")
				return
			}
		}
	}
	return
}

// Set map key value using dot notation keys. Does not overwrite existng nested
// map with basic key value and does not overwrite existing basic key value with map.
func Set(m map[string]interface{}, dotkey string, value interface{}) error {
	keys := strings.Split(dotkey, ".")
	lenKeys := len(keys)
	tmpmap := m
	for i, key := range keys {
		if val, ok := tmpmap[key]; ok {
			if i == lenKeys-1 {
				switch val.(type) {
				case map[string]interface{}:
					return errors.New("\"" + key + "\" already exists of type map[string]interface{}. Can not overwrite existing map.")
				}
				tmpmap[key] = value
			} else {
				var ok bool
				if tmpmap, ok = val.(map[string]interface{}); !ok {
					return errors.New("\"" + key + "\" is not of type map[string]interface{}")
				}
			}
		} else {
			if i == lenKeys-1 {
				tmpmap[key] = value
			} else {
				tmpmap[key] = map[string]interface{}{}
				tmpmap = tmpmap[key].(map[string]interface{})
			}
		}
	}
	return nil
}

// SetForce map key value using dot notation keys. May overwrite existng nested
// map with basic key value and existing basic key value with map.
func SetForce(m map[string]interface{}, dotkey string, value interface{}) {
	keys := strings.Split(dotkey, ".")
	lenKeys := len(keys)
	tmpmap := m
	for i, key := range keys {
		if val, ok := tmpmap[key]; ok {
			if i == lenKeys-1 {
				tmpmap[key] = value
			} else {
				switch val.(type) {
				case map[string]interface{}:
					tmpmap = val.(map[string]interface{})
				default:
					tmpmap[key] = map[string]interface{}{}
					tmpmap = tmpmap[key].(map[string]interface{})
				}
			}
		} else {
			if i == lenKeys-1 {
				tmpmap[key] = value
			} else {
				tmpmap[key] = map[string]interface{}{}
				tmpmap = tmpmap[key].(map[string]interface{})
			}
		}
	}
}

// IsSet returns if a key exists in map.
func IsSet(m map[string]interface{}, dotkey string) bool {
	keys := strings.Split(dotkey, ".")
	lenKeys := len(keys)
	tmpmap := m
	for i, key := range keys {
		val, exist := tmpmap[key]
		if !exist {
			return false
		}
		if i != lenKeys-1 {
			var ok bool
			if tmpmap, ok = val.(map[string]interface{}); !ok {
				return false
			}
		}
	}
	return true
}

// GetIntDefault returns int value. Returns default on error.
func GetIntDefault(m map[string]interface{}, dotkey string, defaultVal int) int {
	if out, err := GetInt(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetInt returns int value and error if any.
func GetInt(m map[string]interface{}, dotkey string) (int, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return 0, err
	}
	return cast.ToIntE(val)
}

// GetInt64Default returns int64 value. Returns default on error.
func GetInt64Default(m map[string]interface{}, dotkey string, defaultVal int64) int64 {
	if out, err := GetInt64(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetInt64 returns int64 value and error if any.
func GetInt64(m map[string]interface{}, dotkey string) (int64, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return 0, err
	}
	return cast.ToInt64E(val)
}

// GetFloat64Default returns float64 value. Returns default on error.
func GetFloat64Default(m map[string]interface{}, dotkey string, defaultVal float64) float64 {
	if out, err := GetFloat64(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetFloat64 returns float64 value and error if any.
func GetFloat64(m map[string]interface{}, dotkey string) (float64, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return 0, err
	}
	return cast.ToFloat64E(val)
}

// GetBoolDefault returns bool value. Returns default on error.
func GetBoolDefault(m map[string]interface{}, dotkey string, defaultVal bool) bool {
	if out, err := GetBool(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetBool returns bool value and error if any.
func GetBool(m map[string]interface{}, dotkey string) (bool, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return false, err
	}
	return cast.ToBoolE(val)
}

// GetStringDefault returns string value. Returns default on error.
func GetStringDefault(m map[string]interface{}, dotkey string, defaultVal string) string {
	if out, err := GetString(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetString returns string value and error if any.
func GetString(m map[string]interface{}, dotkey string) (string, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return "", err
	}
	return cast.ToStringE(val)
}

// GetTimeDefault returns time.Time value. Returns default on error.
func GetTimeDefault(m map[string]interface{}, dotkey string, defaultVal time.Time) time.Time {
	if out, err := GetTime(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetTime returns time.Time value and error if any.
func GetTime(m map[string]interface{}, dotkey string) (time.Time, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return time.Time{}, err
	}
	return cast.ToTimeE(val)
}

// GetDurationDefault returns time.Duration value. Returns default on error.
// Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
func GetDurationDefault(m map[string]interface{}, dotkey string, defaultVal time.Duration) time.Duration {
	if out, err := GetDuration(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetDuration returns time.Duration value and errors if any.
// Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
func GetDuration(m map[string]interface{}, dotkey string) (time.Duration, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return 0, err
	}
	return cast.ToDurationE(val)

}

// GetSliceDefault returns []interface{} value. Returns default on error.
func GetSliceDefault(m map[string]interface{}, dotkey string, defaultVal []interface{}) []interface{} {
	if out, err := GetSlice(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetSlice returns []interface{} value and error if any.
func GetSlice(m map[string]interface{}, dotkey string) ([]interface{}, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return []interface{}{}, err
	}
	return cast.ToSliceE(val)
}

// GetIntSliceDefault returns []int value. Returns default on error.
func GetIntSliceDefault(m map[string]interface{}, dotkey string, defaultVal []int) []int {
	if out, err := GetIntSlice(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetIntSlice returns []int value and error if any.
func GetIntSlice(m map[string]interface{}, dotkey string) ([]int, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return []int{}, err
	}
	return cast.ToIntSliceE(val)
}

// GetStringSliceDefault returns []string value. Returns default on error.
func GetStringSliceDefault(m map[string]interface{}, dotkey string, defaultVal []string) []string {
	if out, err := GetStringSlice(m, dotkey); err == nil {
		return out
	}
	return defaultVal
}

// GetStringSlice returns []string value and error if any.
func GetStringSlice(m map[string]interface{}, dotkey string) ([]string, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return []string{}, err
	}
	return cast.ToStringSliceE(val)
}

// GetStringMap returns map[string]interface{} value and error if any.
func GetStringMap(m map[string]interface{}, dotkey string) (map[string]interface{}, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return map[string]interface{}{}, err
	}
	return cast.ToStringMapE(val)
}

// GetStringMapBool returns map[string]bool value and error if any.
func GetStringMapBool(m map[string]interface{}, dotkey string) (map[string]bool, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return map[string]bool{}, err
	}
	return cast.ToStringMapBoolE(val)
}

// GetStringMapString returns map[string]string value and error if any.
func GetStringMapString(m map[string]interface{}, dotkey string) (map[string]string, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return map[string]string{}, err
	}
	return cast.ToStringMapStringE(val)
}

// GetStringMapStringSlice returns map[string][]string value and error if any.
func GetStringMapStringSlice(m map[string]interface{}, dotkey string) (map[string][]string, error) {
	val, err := Get(m, dotkey)
	if err != nil {
		return map[string][]string{}, err
	}
	return cast.ToStringMapStringSliceE(val)
}
